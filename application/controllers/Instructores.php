<?php
    class Instructores extends CI_Controller
    {
        function __construct()
        {
            parent:: __construct();
            //cargar modelo
            $this->load->model('Instructor');
            $this->load->model('Universidad');
        }
        
        //fucion que renderiza la vista index
        public function index()
        {
            $data['instructores']=$this->Instructor->obtenerTodos();
            //print_r($instructores);///para llamar al arrays
            $this->load->view('header');
            $this->load->view('instructores/index', $data);
            $this->load->view('footer');
        }
        public function indexuni()
        {
            $data['instructores']=$this->Universidad->obtenerTodos1();
            //print_r($instructores);///para llamar al arrays
            $this->load->view('header');
            $this->load->view('instructores/indexuni', $data);
            $this->load->view('footer');
        }
        //fucion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('instructores/nuevo');
            $this->load->view('footer');
        }
        //fucion de empleados
        public function universidad(){
            $this->load->view('header');
            $this->load->view('instructores/universidad');
            $this->load->view('footer');
        }
        public function guardar(){
            $datosNuevoInstructor=array(
                "cedula_ins"=>$this->input->post('cedula_ins'),
                "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
                "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
                "nombre_ins"=>$this->input->post('nombre_ins'),
                "titulo_ins"=>$this->input->post('titulo_ins'),
                "telefono_ins"=>$this->input->post('telefono_ins'),
                "direccion_ins"=>$this->input->post('direccion_ins')
            );
            if($this->Instructor->insertar($datosNuevoInstructor)){///!para un error
                redirect('instructores/index');
            }else{
                echo "<h1>ERROR AL INSERTAR</h1>";///para poner un error
            }
            ///$this->Instructor->insertar($datosNuevoInstructor);
// funcion para eliminar instructores
        }
        public function eliminar($id_ins){
            // echo $id_ins;
            if($this->Instructor->borrar($id_ins)){
                redirect('instructores/index');
            }else{
                echo "ERROR AL BORRAR:(";
            }
        }
        ///funcion que renderiza vista editar con el instrictor
        public function editar($id_ins){
            $data["instructorEditar"]=$this->Instructor->obtenerPorId($id_ins);
            $this->load->view('header');
            $this->load->view('instructores/editar',$data);
            $this->load->view('footer');
        }
        ////actualizar inst
        public function procesarActualizaciones(){
            $datosEditados=array(
                "cedula_ins"=>$this->input->post('cedula_ins'),
                "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
                "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
                "nombre_ins"=>$this->input->post('nombre_ins'),
                "titulo_ins"=>$this->input->post('titulo_ins'),
                "telefono_ins"=>$this->input->post('telefono_ins'),
                "direccion_ins"=>$this->input->post('direccion_ins')
            );
        $id_ins=$this->input->post("id_ins");
        if($this->Instructor->actualizar($id_ins,$datosEditados)){
            redirect("instructores/index");
        }else{
            echo "ERROR AL ACTULIZAR :(";
        }
        }


        // nuevo insercion
        public function guardar1(){
            $datosNuevoUniversidad=array(
                "nombre_uni"=>$this->input->post('nombre_uni'),
                "direccion_uni"=>$this->input->post('direccion_uni'),
                "carrera_uni"=>$this->input->post('carrera_uni'),
                "pagina_uni"=>$this->input->post('pagina_uni'),
                "facultad_uni"=>$this->input->post('facultad_uni'),
                "nombre_rector_uni"=>$this->input->post('nombre_rector_uni'),
            );
            $this->Universidad->insertar($datosNuevoUniversidad);
        }
            // echo $this->input->post('cedula_ins');
            // echo "<br>";
            // echo $this->input->post('primer_apellido_ins');
            // echo "<br>";
            // echo $this->input->post('segundo_apellido_ins');
            // echo "<br>";
            // echo $this->input->post('nombres_ins');
            // echo "<br>";
            // echo $this->input->post('titulo_ins');
            // echo "<br>";
            // echo $this->input->post('telefono_ins');
            // echo "<br>";
            // echo $this->input->post('direccion_ins');
        public function eliminar1($id_uni){
                // echo $id_ins;
            if($this->Universidad->borrar1($id_uni)){
                redirect('instructores/indexuni');
            }else{
                echo "ERROR AL BORRAR:(";
            }
            }
        
    }////cierre de clase
        
?>
