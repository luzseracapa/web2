<?php
   class Universidad extends CI_Model
   {
    function __Contructor()
    {
        parent::__constructor();
    }
    ////funcion para insertar instructor de mysql
    function insertar($datos){
        ////consultar active record en codeigniter inyeccion sql
        return $this->db->insert("universidad", $datos);
    }
    //funcion para consultar instructores
    function obtenerTodos1(){
        $listadoInstructores=$this->db->get("universidad");///llamar o consultar datos
        if($listadoInstructores->num_rows()>0){///validar datos, si hay datos,comprobacion con if y else
            return $listadoInstructores->result();
        }else{//no hay datos
            return false;
        }
    }
    ////borrar instructores
    function borrar1($id_uni){
        $this->db->where("id_uni",$id_uni);
        if($this->db->delete("universidad")){
            return true;
        } else{
            return false;
        }
    }
   }//cire de la clase

?>