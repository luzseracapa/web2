<?php
   class Instructor extends CI_Model
   {
    function __Contructor()
    {
        parent::__constructor();
    }
    ////funcion para insertar instructor de mysql
    function insertar($datos){
        return $this->db->insert("instructor", $datos);
    }

    //funcion para consultar instructores
    function obtenerTodos(){
        $listadoInstructores=$this->db->get("instructor");///llamar o consultar datos
        if($listadoInstructores->num_rows()>0){///validar datos, si hay datos,comprobacion con if y else
            return $listadoInstructores->result();
        }else{//no hay datos
            return false;
        }
    }
    ////borrar instructores
    function borrar($id_ins){
        $this->db->where("id_ins",$id_ins);
        if($this->db->delete("instructor")){
            return true;
        } else{
            return false;
        }
    }
    ///funcion para consultar un instructor especifico
    function obtenerPorId($id_ins){
        $this->db->where("id_ins",$id_ins);
        $instructor=$this->db->get("instructor");
        if($instructor->num_rows()>0){
            return $instructor->row();
        }
        return false;
    }
    ////funcion para actualizar
    function actualizar($id_ins,$datos){
        $this->db->where("id_ins",$id_ins);
        return $this->db->update('instructor',$datos);
    }

   }//cire de la clase

?>