<h1>NUEVO INSTRUCTOR</h1>
<form class=""
id="frm_nuevo_instructor" 
action="<?php echo site_url(); ?>/instructores/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          required
          name="cedula_ins" value="" id="cedula_ins">
      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          required
          name="primer_apellido_ins" value="" id="primer_apellido_ins">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_ins" value="" id="segundo_apellido_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control"
          required
          name="nombre_ins" value="" id="nombre_ins">
      </div>
      <div class="col-md-4">
          <label for="">Título:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          required
          name="titulo_ins" value="" id="titulo_ins">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:<span class="obligatorio">(Obligatorio)</span></label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control"
        required
        name="telefono_ins" value="" id="telefono_ins">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:<span class="obligatorio">(Obligatorio)</span></label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          required
          name="direccion_ins" value="" id="direccion_ins">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
  $("#frm_nuevo_instructor").validate({
    rules:{
      cedula_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      primer_apellido_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      nombre_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      titulo_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      telefono_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      direccion_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },

    },
    messages:{
      cedula_ins:{
        required:"Ingrese el numero de cedula por favor",
        minlength:"Cedula incorecta, ingrese los digitos correspondientes",
        maxlength:"Cedula incorecta, ingrese los digitos correspondientes",
        digits:"Este campo solo acepta numeros",
        number:"Este campo solo acepta numeros"
      },
      primer_apellido_ins:{
        required:"Ingrese su apellido por favor",
        minlength:"Ingrese el apellido al menos de 3 digitos",
        maxlength:"Apellido incorrecto",
        letras:"Este campo acepta solo letras"
      },
      nombre_ins:{
        required:"Ingrese su nombre por favor",
        minlength:"Ingrese el nombre al menos de 3 digitos",
        maxlength:"Nombre incorrecto",
        letras:"Este campo acepta solo letras"
      },
      titulo_ins:{
        required:"Ingrese su nombre del titulo por favor",
        minlength:"Ingrese el nombre del titulo al menos de 3",
        maxlength:"Titulo incorrecto",
        letras:"Este campo acepta solo letras"
      },
      telefono_ins:{
        required:"Ingrese su telefono por favor",
        minlength:"Ingrese su telefono, al menos 10 digitos correspondientes",
        maxlength:"Telefono incorrecto",
        digits:"Este campo solo acepta numeros",
        number:"Este campo solo acepta numeros"
      },
      direccion_ins:{
        required:"Ingrese su direccion por favor",
        minlength:"Ingrese su direccion correcto al menos de 5",
        maxlength:"Direccion incorrecto",
        letras:"Este campo acepta solo letras"
      },
      

    }
  });
</script>


