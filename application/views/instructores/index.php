<div class="row">
    <div class="col-md-8">
    <h1>LISTADO DE INSTRUCTORES</h1>
    </div>
    <div class="col-md-4"> <br>
        <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Instructor
       </a>
    </div>
</div>


<br>
<?php if ($instructores): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>CEDULA</th>
                <th>PRIMER APELLIDO</th>
                <th>SEGUNDO APELLIDO</th>
                <th>NOMBRE</th>
                <th>TITULO</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instructores as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cedula_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->primer_apellido_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->segundo_apellido_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->titulo_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_ins;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_ins;?>
                    </td>
                    <!-- para poner un lapz dentro de un forech para poder editar -->
                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/instructores/editar/<?php echo $filaTemporal->id_ins;?>"
                        title="Editar Instructor" style="color:yellow">
                            <!-- <i class="glyphicon glyphicon-pencil">Editar</i> -->
                            <i class="mdi mdi-pencil">Editar</i>
                        </a>
                        
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->id_ins;?>" title="Eliminar Instructor" 
                        onclick="return confirm('Esta seguro de eliminar al instructor');"
                        style="color:red">
                            <!-- <i class="glyphicon glyphicon-trash">Eliminar</i> -->
                            <i class="mdi mdi-close">Eliminar</i>
                        </a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <?php print_r($instructores);?> -->
<?php else: ?>
    <h1>No hay datos</h1>
    <?php endif; ?>