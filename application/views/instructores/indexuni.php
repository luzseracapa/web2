<div class="row">
    <div class="col-md-8">
    <h1>LISTADO DE UNIVERSIDADES</h1>
    </div>
    <div class="col-md-4"> <br>
        <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Universidad
       </a>
    </div>
</div>

<br>
<?php if ($instructores): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCION</th>
                <th>CARRERA</th>
                <th>PAGINA</th>
                <th>FACULTAD</th>
                <th>NOMBRE RECTOR</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instructores as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->carrera_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->pagina_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->facultad_uni;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_rector_uni;?>
                    </td>
                    <!-- para poner un lapz dentro de un forech para poder editar -->
                    <td class="text-center">
                        <a href="#" title="Editar Instructor" style="color:black">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/instructores/eliminar1/<?php echo $filaTemporal->id_uni;?>" title="Eliminar Instructor" 
                        onclick="return confirm('Esta seguro de eliminar al instructor');"
                        style="color:red">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <?php print_r($instructores);?> -->
<?php else: ?>
    <h1>No hay datos de universidades</h1>
    <?php endif; ?>